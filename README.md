# Mensajito Widget de Pagina

Este proyecto usa [Create React App](https://github.com/facebook/create-react-app) y [Contentful](https://github.com/contentful/contentful.js) para sus datos

## Usar

Incluir el script via CDN y montar la instancia con tu configuración

```
<script src="https://mensajito.s3-us-west-1.amazonaws.com/index.js" />
<script>
  Mensajito.mount({
    url: 'http://mensajito.mx:8000/f79bbdbe728b'
  });
</script>
```

### Configuración Completo

| Opción          |                                   |
| --------------- | --------------------------------- |
| url             | Mensajito Streaming URL           |
| logoUrl         | Logo Url                          |
| nombre          | Nombre de estacion                |
| informacion     | Mas informacion sobre el estacion |
| sliderImageUrls | URLs de imagenes para slider      |
| instagram       | Instagram URL                     |
| mixcloud        | Mixcloud URL                      |
| soundcloud      | Soundcloud URL                    |
| website         | Website URL                       |

```
Mensajito.mount({
  url: "http://mensajito.mx:8000/f79bbdbe728b",
  logoUrl:
    "//images.ctfassets.net/kuozvi4wnq42/56x5JMxCMDaynvVzglkCTh/8c27942b72d29b2a7ade51e28d342665/movelike-logo.jpg",
  nombre: "Movelike",
  informacion:
    "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu.",
  sliderImageUrls: [
    "//images.ctfassets.net/kuozvi4wnq42/RbzYPOO00cfAx2ZapcIST/f98b3f3bfa234df962afe3c23b5f8c9f/movelike-slide-1.jpg",
    "//images.ctfassets.net/kuozvi4wnq42/5s8JWshKplTf2oER6JJpPJ/f7f187bb9d2c7bece7237373b4277647/movelike-slide-image-2.jpg"
  ],
  instagram: "http://instagram.com/movelike.co",
  mixcloud: "http://mixcloud.com/movelikeco",
  soundcloud: "http://soundcloud.com/movelikeco",
  website: "http://movelike.co"
})
```

## Empezar el Proyecto

`git clone git@gitlab.com:mensajito/page-widget.git`

`cd page-widget`

`npm install`

`npm start`

`open localhost:30000`

### Pruebas

`npm test`
