import React from 'react'

const Slider = ({ imageUrls }) => {

  const sliderItem = imageUrls.map(url => (
    <div
      className="home__slider--item"
      key={url}
      style={{backgroundImage: `url(${url})`}}>
      <span></span>
    </div>
  ))

  return (
    <div className="home__slider--holder">
      <div className="home__slider slider_JS">
        {sliderItem}
      </div>
    </div>
  )
}

export default Slider
