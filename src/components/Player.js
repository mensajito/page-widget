import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlay, faPause } from '@fortawesome/free-solid-svg-icons';

const useAudio = url => {

  const [audio] = useState(new Audio(url));
  const [playing, setPlaying] = useState(false);

  const toggle = () => setPlaying(!playing);

  useEffect(
    () => {
      playing ? audio.play() : audio.pause();
    },
    [playing]
  );

  return [playing, toggle];
};

const Player = ({ url }) => {

  const [playing, toggle] = useAudio(url);

  const PlayerContent = playing ?
    <p>Detener <FontAwesomeIcon icon={faPause} /></p>
    :
    <p>Reproducir <FontAwesomeIcon icon={faPlay} /></p>

  return (
    <div
      onClick={toggle}
      className="home__main--player block player_btn_JS">
      { PlayerContent }
    </div>
  );
};

export default Player;
