import React, { useState } from 'react'
import classNames from 'classnames'
import '../styles/page.css'

const DescriptionWidget = ({ nombre, informacion }) => {

  const [isMenuActive, setMenuState] = useState(false);

  const buttonClasses = classNames({
    'home__info--btn': true,
    'info_btn_JS': true,
    'active': isMenuActive
  })

  const contentClasses = classNames({
    'home__info--content': true,
    'info_JS': true,
    'active': isMenuActive
  })

  return (
    <div className="home__info">
      <div className={contentClasses}>
        <div className="home__info--ttl">{nombre}</div>
        <div className="home__info--txt">{informacion}</div>
      </div>
      <div
        onClick={() => setMenuState(!isMenuActive)}
        className={buttonClasses}>
      </div>
    </div>
  )
}

export default DescriptionWidget
