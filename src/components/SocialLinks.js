import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faGlobe } from '@fortawesome/free-solid-svg-icons';
import { faInstagram, faSoundcloud, faMixcloud } from '@fortawesome/free-brands-svg-icons';

const Contact = ({ website, soundcloud, mixcloud, instagram }) => {

  return (
    <div className="home__social">
      <a
        target="_blank"
        href={website}
        className="home__social--item">
        <FontAwesomeIcon icon={faGlobe} />
      </a>

      <a
        target="_blank"
        href={soundcloud}
        className="home__social--item">
        <FontAwesomeIcon icon={faSoundcloud} />
      </a>

      <a
        target="_blank"
        href={mixcloud}
        className="home__social--item">
        <FontAwesomeIcon icon={faMixcloud} />
      </a>

      <a
        target="_blank"
        href={instagram}
        className="home__social--item">
        <FontAwesomeIcon icon={faInstagram} />
      </a>
    </div>
  )
}

export default Contact
