import React from 'react'
import { Link } from "react-router-dom";

const StationCard = ({ title, link, imageUrl }) => {

  return (
    <div
      style={{
        width: 300,
        height: 200
      }}
    >
      <Link
        to={link} >
        <img
          style={{
            maxWidth: `100%`,
            maxHeight: `100%`
          }}
          src={imageUrl} />
          {title}
      </Link>
    </div>
  )
}

export default StationCard
