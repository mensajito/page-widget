import React from 'react';
import SocialLinks from './components/SocialLinks';
import InformacionWidget from './components/InformacionWidget';
import Player from './components/Player';
import Slider from './components/Slider';
import './styles/page.css';
import styled from 'styled-components';

const Home = styled.section`
  position:relative;
`

const LogoImg = styled.img`
  max-width: 100%;
  height: auto;
  border: 10px solid #000000;
  cursor: pointer;
  display: block;
  margin-bottom: 1px;
  background-color: #ffffff;
  -webkit-transition: all 0.3s ease;
  transition: all 0.3s ease;
`

const PlayerContainer = styled(Player)``

const Main = styled.section`
  width: 180px;
  z-index: 1;
  position: relative;

  &:hover ${LogoImg} {
    border: 10px solid #101010;
  }
  &:hover ${PlayerContainer} {
    background-color: #101010;
  }
`

const Container = styled.div`
  height: 100vh;
  padding: 60px;
  display: -webkit-box;
  display: -webkit-flex;
  display: flex;
  max-width: 100%;
  -webkit-flex-wrap: wrap;
  flex-wrap: wrap;
  -webkit-box-align: start;
  -webkit-align-items: flex-start;
  align-items: flex-start;

  @media (max-width: 1080px) {
    padding: 50px;
  }
  @media (max-width: 900px) {
    padding: 40px;
    height: auto;
  }
  @media (max-width: 720px) {
    padding: 30px;
  }
  @media (max-width: 600px) {
    padding: 22px;
  }
`

const App = ({
  nombre,
  website,
  informacion,
  instagram,
  mixcloud,
  sliderImageUrls,
  soundcloud,
  url,
  logoUrl
}) => {

  return (
		<Home>
			<Container>
				<Main>
					<LogoImg src={logoUrl} alt="" />
          <PlayerContainer url={url} />
					<div className="home__main--dots slider_dots_JS"></div>
				</Main>

          <Slider
            imageUrls={sliderImageUrls}
          />
          <SocialLinks
            website={website}
            mixcloud={mixcloud}
            soundcloud={soundcloud}
            instagram={instagram}
          />
          <InformacionWidget
            nombre={nombre}
            informacion={informacion}
          />
			</Container>
		</Home>
  )
}

export default App
