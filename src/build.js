import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

const insertAfter = (referenceNode, newNode) => {
  referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}

export default class EmbeddableApp {
  static el

  static mount({ appendAfter = null, parentElement = null, ...props } = {}) {
    function doRender() {
      if (EmbeddableApp.el) {
        throw new Error('EmbeddableApp is already mounted, unmount first');
      }

      const el = document.createElement('div');

      if (appendAfter) {
        insertAfter(document.querySelector(appendAfter), el)
      } else if (parentElement) {
        document.querySelector(parentElement).appendChild(el);
      } else {
        document.body.appendChild(el);
      }

      ReactDOM.render(
        <App {...props} />,
        el
      );

      EmbeddableApp.el = el;
    }

    if (document.readyState === 'complete') {
      doRender();
    } else {
      window.addEventListener('load', () => {
        doRender();
      });
    }
  }

  static unmount() {
    if (!EmbeddableApp.el) {
      throw new Error('EmbeddableApp is not mounted, mount first');
    }

    ReactDOM.unmountComponentAtNode(EmbeddableApp.el);
    EmbeddableApp.el.parentNode.removeChild(EmbeddableApp.el);

    EmbeddableApp.el = null;
    EmbeddableApp.store = null;
  }
}
