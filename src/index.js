import React from 'react'
import ReactDOM from 'react-dom'
import * as serviceWorker from './serviceWorker'
import App from './App'

const config = {
  url: 'http://mensajito.mx:8000/f79bbdbe728b',
  logoUrl: '//images.ctfassets.net/kuozvi4wnq42/zYc2N0zJzkveDa5Omqqao/23f16fd2e97a949ce764eaa877c788ca/bodega.png',
  nombre: 'Davin Radio',
  informacion: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu.',
  sliderImageUrls: [
    '//images.ctfassets.net/kuozvi4wnq42/RbzYPOO00cfAx2ZapcIST/f98b3f3bfa234df962afe3c23b5f8c9f/movelike-slide-1.jpg',
    '//images.ctfassets.net/kuozvi4wnq42/5s8JWshKplTf2oER6JJpPJ/f7f187bb9d2c7bece7237373b4277647/movelike-slide-image-2.jpg'
  ],
  instagram: 'http://instagram.com/movelike.co',
  mixcloud: 'http://mixcloud.com/movelikeco',
  soundcloud: 'http://soundcloud.com/movelikeco',
  website: 'http://movelike.co',
}

ReactDOM.render(
  <App {...config} />,
  document.getElementById(`root`)
)

serviceWorker.unregister()
